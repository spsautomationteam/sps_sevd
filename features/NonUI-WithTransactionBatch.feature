@non_ui
Feature: Non Ui with transaction batch test
  Random test to make sure things work.

  Scenario Outline: making a Non Ui Request that requires a non-empty transaction batch
    Given that I have added a <transaction_type> transaction
    And that the test data from <test_data> is used
    And that I secure the test data
    When I process the request
    Then The response code should be 200
    And The response body should not be empty
    And the response should not be "Invalid Request Data"
    And the response should not be "Internet Connection Error"
    And the response's <test_tag> tag should include "<test_tag_content>"
  @ach
    Examples:
      | test_data | test_tag        | test_tag_content    | transaction_type |
      | TC_5      | Count           | 1                   | ACH              |
      | TC_8      | ResponseMessage | BATCH 1 OF 1 CLOSED | ACH              |
  @cc
    Examples:
      | test_data | test_tag        | test_tag_content    | transaction_type |
      | TC_4      | Count           | 1                   | credit card      |
      | TC_21     | ResponseMessage | BATCH 1 OF 1 CLOSED | credit card      |