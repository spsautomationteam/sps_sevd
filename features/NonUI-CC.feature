@cc @non_ui
Feature: Non Ui CC test
  Random test to make sure things work.

  Scenario Outline: making a Non Ui CC Request
    Given that the test data from <test_data> is used
    And that the GUID tag in the test data is the configured guidCC value
    And that I have generated a new Reference1 value
    And that I secure the test data
    When I process the request
    Then The response code should be 200
    And The response body should not be empty
    And the response should not be "Invalid Request Data"
    And the response should not be "Internet Connection Error"
    And the response's <test_tag> tag should include "<test_tag_content>"
    Examples:
      | test_data | test_tag        | test_tag_content    |
      | TC_11     | TransactionID   | CCTest              |
      ##| TC_14     | ResponseMessage | APPROVED            |
      | TC_16     | ResponseMessage | APPROVED            |
      | TC_19     | ResponseMessage | NO TRANSACTIONS     |
      | TC_22     | ResponseMessage | APPROVED            |