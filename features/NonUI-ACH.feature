@ach @non_ui
Feature: Non Ui ACH test
  Random test to make sure things work.

  Scenario Outline: making a Non Ui ACH Request
    Given that the test data from <test_data> is used
    And that the GUID tag in the test data is the configured guidAch value
    And that I have generated a new Reference1 value
    And that I secure the test data
    When I process the request
    Then The response code should be 200
    And The response body should not be empty
    And the response should not be "Invalid Request Data"
    And the response should not be "Internet Connection Error"
    And the response's <test_tag> tag should include "<test_tag_content>"
    Examples:
      | test_data | test_tag        | test_tag_content    |
      | TC_1      | ResponseMessage | ACCEPTED            |
      | TC_2      | ResponseMessage | ACCEPTED            |
#      | TC_3      | ResponseMessage | APPROVED            |
      | TC_9      | ResponseMessage | SUCCESS             |
      | TC_10     | ResponseMessage | SUCCESS             |
      | TC_12     | TransactionID   | ACHTest             |
      | TC_13     | ResponseMessage | SUCCESS             |
      | TC_15     | ResponseMessage | ACCEPTED            |
      | TC_18     | ResponseMessage | NO TRANSACTIONS     |
      | TC_20     | ResponseMessage | NO TRANSACTIONS     |