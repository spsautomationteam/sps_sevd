require 'nokogiri'

Before('@ach') do
  Cucumber.logger.debug("Checking for open batches...\n")
  check_test_data = FileUtils.get_non_ui_test_data_file(Config['queryBatchTestCase']['ach'])
  secure_request = HTTPUtils.post_api_request('frmEnvelope.aspx', request: check_test_data)
  response = HTTPUtils.post_api_request('frmPayment.aspx', request: secure_request)
  response_xml = Nokogiri::XML(response.body)
  batch = response_xml.at_xpath('//Count')
  if batch.content.to_i > 0
    close_test_data = FileUtils.get_non_ui_test_data_file(Config['closeBatchTestCase']['ach'])
    secure_request = HTTPUtils.post_api_request('frmEnvelope.aspx', request: close_test_data)
    response = HTTPUtils.post_api_request('frmPayment.aspx', request: secure_request)
    Cucumber.logger.debug("Closing batches...\n")
    Cucumber.logger.debug("Close batch response:\n#{response.body}\n")
  else
    Cucumber.logger.debug("No batches to close.\n")
  end
end

Before('@cc') do
  Cucumber.logger.debug("Checking for open batches...\n")
  check_test_data = FileUtils.get_non_ui_test_data_file(Config['queryBatchTestCase']['cc'])
  secure_request = HTTPUtils.post_api_request('frmEnvelope.aspx', request: check_test_data)
  response = HTTPUtils.post_api_request('frmPayment.aspx', request: secure_request)
  response_xml = Nokogiri::XML(response.body)
  batch = response_xml.at_xpath('//Count')
  if batch.content.to_i > 0
    close_test_data = FileUtils.get_non_ui_test_data_file(Config['closeBatchTestCase']['cc'])
    secure_request = HTTPUtils.post_api_request('frmEnvelope.aspx', request: close_test_data)
    response = HTTPUtils.post_api_request('frmPayment.aspx', request: secure_request)
    Cucumber.logger.debug("Closing batches...\n")
    Cucumber.logger.debug("Close batch response:\n#{response.body}\n")
  else
    Cucumber.logger.debug("No batches to close.\n")
  end
end

Given(/^that the test data from (.*) is used$/) do |file_name|
  Cucumber.logger.debug("Using test data file #{file_name}\n")
  @test_data = FileUtils.get_ui_test_data_file(file_name)
  @test_data ||= FileUtils.get_non_ui_test_data_file(file_name)
  @current_test_data = file_name
end

Given(/^that the (.*) tag in the test data is "(.*)"$/) do |tag_xpath, tag_content|
  elem = @test_data.at_xpath("//#{tag_xpath}") || @test_data.at_xpath(tag_xpath)
  elem.content = tag_content unless elem.nil?
end

Given(/^that the (.*) tag in the test data is the configured (.*) value$/) do |tag_xpath, config_key| # rubocop:disable Metrics/LineLength
  step("that the #{tag_xpath} tag in the test data is \"#{Config[config_key]}\"")
end

Given(/^that the (.*) tag in the test data is the stored (.*) value$/) do |tag_xpath, store_key|
  step("that the #{tag_xpath} tag in the test data is " +
         "\"#{VarUtils.get_stored_variable(store_key)}\"")
end

Given(/^that I secure the test data$/) do
  @secure_test_data = HTTPUtils.post_api_request('frmEnvelope.aspx', request: @test_data)
end

Given(/^that I have added an? (ACH|credit card) transaction$/) do |transaction_type|
  Cucumber.logger.debug("Adding transaction...\n")
  is_ach = transaction_type == 'ACH'
  add_transaction_key = is_ach ? 'ach' : 'cc'
  guid_key = is_ach ? 'guidAch' : 'guidCC'
  step("that the test data from #{Config['addTransaction'][add_transaction_key]} is used")
  step('that I have generated a new Reference1 value')
  step("that the GUID tag in the test data is the configured #{guid_key} value")
  step('that I secure the test data')
  step('I process the request')
  step('The response code should be 200')
  step('The response body should not be empty')
  step('the response should not be "Invalid Request Data"')
end

Given(/^that I have stored the (\S*) tag from the response(?: as (\S*))?$/) do |tag_xpath, store_name| # rubocop:disable Metrics/LineLength
  response_xml = Nokogiri::XML(@response.body)
  tag = response_xml.at_xpath("//#{tag_xpath}") || response_xml.at_xpath(tag_xpath)
  VarUtils.set_stored_variable(store_name || tag_xpath, tag.content)
end

Given(/^that I have generated a new Reference1 value$/) do
  new_reference = "INV# #{[*('0'..'9')].sample(9).join}"
  step("that the Reference1 tag in the test data is \"#{new_reference}\"")
end

When(/^I process the request$/) do
  Cucumber.logger.warn('The test data was not secured before sending') if @secure_test_data.nil?
  @response = HTTPUtils.post_api_request('frmPayment.aspx',
                                         request: @secure_test_data || @test_data)
end

Then(/^the response should( not)? be "(.*)"$/) do |not_found, check_content|
  if not_found.nil?
    expect(@response.body).to eq(check_content)
  else
    expect(@response.body).not_to eq(check_content)
  end
end

Then(/^the response should( not)? include "(.*)"$/) do |not_found, check_content|
  if not_found.nil?
    expect(@response.body).to include(check_content)
  else
    expect(@response.body).not_to include(check_content)
  end
end

Then(/^print the( secure)? test data$/) do |secure_found|
  data = secure_found.nil? ? @test_data : @secure_test_data
  Cucumber.logger.debug("Test data is nil: #{data.nil?}\n")
  Cucumber.logger.debug("Test data:\n#{data}\n")
end

Then(/^print the response$/) do
  Cucumber.logger.debug("Response is nil: #{@response.nil?}\n")
  Cucumber.logger.debug("Response:\n#{@response}\n")
end

Then(/^the response's (.*) tag should be "(.*)"/) do |tag_xpath, tag_content|
  response_xml = Nokogiri::XML(@response.body)
  elem = response_xml.at_xpath(tag_xpath) || response_xml.at_xpath("//#{tag_xpath}")
  expect(elem.content.strip).to eq(tag_content)
end

Then(/^the response's (.*) tag should include "(.*)"/) do |tag_xpath, tag_content|
  response_xml = Nokogiri::XML(@response.body)
  elem = response_xml.at_xpath(tag_xpath) || response_xml.at_xpath("//#{tag_xpath}")
  expect(elem.content.strip).to include(tag_content)
end