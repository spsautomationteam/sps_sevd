@non_ui @ach
Feature: Credit Request
  Uh, this doesn't work yet. Apparently there's no internet connection...

  Scenario: Credit Request
    Given that I have added an ACH transaction
    And that I have stored the VANReference tag from the response
    And that the test data from TC_3 is used
    And that the VANReference tag in the test data is the stored VANReference value
    And that I secure the test data
    When I process the request
    Then The response code should be 200
    And The response body should not be empty
    And the response should not be "Invalid Request Data"
    And the response should not be "Internet Connection Error"
    And the response's ResponseMessage tag should include "APPROVED"
