require 'base64'
require 'cgi'
require 'openssl'
require 'rest-client'

require_relative './config'

# Various utility functions related to authentication used for testing.
module AuthUtils

  # Generates a HMAC-SHA1 authentication code.
  #
  # @param data [String] The data to be digested.
  # @return [String] The HMAC-SHA1 authentication code.
  def self.hmac_sha1(data)
    CGI.escape(
      Base64.encode64(OpenSSL::HMAC.digest('sha1', Config['merchantKey'], data))
    ).to_s
  end

  # Generates the authentication header for HTTP requests.
  #
  # @param verb [String] The HTTP verb being used for the HTTP request.
  # @param url [String] The URL that the HTTP request is using.
  # @param body [String] The body of the request being made.
  #   This is only required for any request that have a body.
  # @return [Object] The header object to use for the HTTP request.
  def self.generate_authentication_header(verb, url, body = '')
    {
      Authentication: Config['merchantId'].to_s + ':' +
        hmac_sha1(verb.to_s.upcase + url.to_s.downcase + body.to_s)
    }
  end

  # Generates the authentication header for a GET HTTP request.
  #
  # @param url [String] The URL that the HTTP request is using.
  # @return [Object] The header object to use for the HTTP request.
  def self.get_authentication_header(url)
    generate_authentication_header('GET', url)
  end

  # Generates the authentication header for a POST HTTP request.
  #
  # @param url [String] The URL that the HTTP request is using.
  # @param body [String] The body of the HTTP request.
  # @return [Object] The header object to use for the HTTP request.
  def self.post_authentication_header(url, body)
    generate_authentication_header('POST', url, body)
  end

  # Generates the authentication header for a PUT HTTP request.
  #
  # @param url [String] The URL that the HTTP request is using.
  # @param body [String] The body of the HTTP request.
  # @return [Object] The header object to use for the HTTP request.
  def self.put_authentication_header(url, body)
    generate_authentication_header('PUT', url, body)
  end

  # Generates the authentication header for a DELETE HTTP request.
  #
  # @param url [String] The URL that the HTTP request is using.
  # @return [Object] The header object to use for the HTTP request.
  def self.delete_authentication_header(url)
    generate_authentication_header('DELETE', url)
  end

end