# Various utility functions related to files used for testing
module FileUtils

  # Retrieves a test data file and parses it as XML.
  #
  # @param file_name [String] The name of the file (without the extension) in the `test_data`
  #   folder. If the string ends in `'.xml'` it assumes that the string is a path to the file.
  # @return [Nokogiri::XML] The content of the test data file parsed as XML.
  def self.get_test_data_file(file_name)
    path = if file_name.end_with?('.xml')
             file_name
           else
             File.join('test_data', file_name + '.xml')
           end
    return nil unless File.exist?(path)
    # Cucumber.logger.debug("Loading test data file #{path}\n")
    xml = File.open(path) { |f| Nokogiri::XML(f) }
    if xml.errors.any?
      raise "The XML in file #{path} resulted in errors while parsing it:\n
            #{xml.errors.join("\n")}"
    end
    xml
  end

  # Retrieves a ui test data file and parses it as XML.
  #
  # @param file_name [String] The name of the file (without the extension) in the `test_data/ui`
  #   folder. If the string ends in `'.xml'` it assumes that the string is a path to the file.
  # @return [Nokogiri::XML] The content of the test data file parsed as XML.
  #
  # @see get_test_data_file
  def self.get_ui_test_data_file(file_name)
    path = if file_name.end_with?('.xml')
             file_name
           else
             File.join('test_data', 'ui', file_name + '.xml')
           end
    get_test_data_file(path)
  end

  # Retrieves a non ui test data file and parses it as XML.
  #
  # @param file_name [String] The name of the file (without the extension) in the `test_data/non_ui`
  #   folder. If the string ends in `'.xml'` it assumes that the string is a path to the file.
  # @return [Nokogiri::XML] The content of the test data file parsed as XML.
  #
  # @see get_test_data_file
  def self.get_non_ui_test_data_file(file_name)
    path = if file_name.end_with?('.xml')
             file_name
           else
             File.join('test_data', 'non_ui', file_name + '.xml')
           end
    get_test_data_file(path)
  end

end