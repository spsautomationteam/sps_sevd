
# Various utility functions related to variables used for testing.
module VarUtils

  @stored_vars = {}

  # Stores a value with a specific key.
  #
  # @param key [String] The key to store the value under.
  # @param value [Object] The object to store.
  def self.set_stored_variable(key, value)
    @stored_vars[key] = value
  end

  # Retrieves a stored value. If the key does not exist nil is returned.
  #
  # @param key [String] The key of the value to lookup.
  # @return [Object] The object stored under the passed key.
  def self.get_stored_variable(key)
    @stored_vars[key]
  end

end